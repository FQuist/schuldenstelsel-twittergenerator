

<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=yes"/>
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <script type="text/javascript">
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement("style")
            msViewportStyle.appendChild(
                document.createTextNode(
                    "@-ms-viewport{width:auto!important}"
                )
            )
            document.getElementsByTagName("head")[0].appendChild(msViewportStyle)
        }
    </script>

    <!-- BEGIN Tynt Script -->
    <script type="text/javascript">
        if(document.location.protocol=='http:'){
            var Tynt=Tynt||[];Tynt.push('cDJciW114r4PGzacwqm_6r');
            (function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
        }
    </script>
    <!-- END Tynt Script -->



    <base href="http://www.lsvb.nl/" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="lsvb, landelijke studenten vakbond, studentenvakbond, studenten, student, studeren, universiteit, hogeschool, medezeggenschap, opleiding, studie, onderwijs, hoger onderwijs" />
    <meta name="rights" content="Alle rechten voorbehouden. Enige en strikte uitzondering hierop is het (ten dele) overnemen of bewerken van deze content voor niet-commercieel educatief gebruik. Bijvoorbeeld door docenten voor eigen lesmateriaal, of door leerlingen voor eigen werkstukken." />
    <meta name="title" content="Landelijke Studenten Vakbond" />
    <meta property="og:url" content="http://lsvb.nl/" />
    <meta property="og:title" content="Landelijke Studenten Vakbond - LSVb - Voor heel studerend Nederland" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="http://lsvb.nl/media/k2/items/cache/29766e2a37e979b18d18c428ff9c5aba_M.jpg" />
    <meta name="image" content="http://lsvb.nl/media/k2/items/cache/29766e2a37e979b18d18c428ff9c5aba_M.jpg" />
    <meta property="og:description" content="De Landelijke Studenten Vakbond is de belangenbehartiger van meer dan een half miljoen studenten." />
    <meta name="description" content="De Landelijke Studenten Vakbond is de belangenbehartiger van meer dan een half miljoen studenten." />
    <meta name="generator" content="Joomla! - Open Source Content Management" />
    <title>Landelijke Studenten Vakbond - LSVb - Voor heel studerend Nederland</title>
    <link href="http://lsvb.nl/" rel="canonical" />
    <link href="/templates/bisous/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <link rel="stylesheet" href="/media/system/css/modal.css" type="text/css" />
    <link rel="stylesheet" href="/plugins/system/akmarkdown/assets/css/content.css" type="text/css" />
    <link rel="stylesheet" href="/templates/bisous/css/lsvbstrap/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="/templates/bisous/css/template.css" type="text/css" />
    <script src="/media/system/js/mootools-core.js" type="text/javascript"></script>
    <script src="/media/jui/js/jquery.min.js" type="text/javascript"></script>
    <script src="/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
    <script src="/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="/media/system/js/core.js" type="text/javascript"></script>
    <script src="/media/system/js/mootools-more.js" type="text/javascript"></script>
    <script src="/media/system/js/modal.js" type="text/javascript"></script>
    <script src="/components/com_k2/js/k2.js?v2.6.8&amp;sitepath=/" type="text/javascript"></script>
    <script src="/plugins/system/akmarkdown/assets/js/content.js" type="text/javascript"></script>
    <script src="/templates/bisous/js/lsvbstrap/bootstrap.min.js" type="text/javascript"></script>
    <script src="/templates/bisous/js/respond.min.js" type="text/javascript"></script>
    <script type="text/javascript">

        jQuery(function($) {
            SqueezeBox.initialize({});
            SqueezeBox.assign($('a.modal').get(), {
                parse: 'rel'
            });
        });
        var AKMarkdownOption = { Article_ForceNewWindow: 1, Article_NavList: false, Article_NavList_Class: 'none', Article_ForceImageAlign: 'center', Article_ForceImageMaxWidth: 0, Article_ImageClass: 'img-responsive', Article_TableClass: 'none'}; AKMarkdownPretiffy( AKMarkdownOption );
    </script>
    <script type="text/javascript">
        (function() {
            var strings = {"PLG_SYSTEM_AKMARKDOWN_NAV_LIST_BACK_TO_TOP":"Back To Top"};
            if (typeof Joomla == 'undefined') {
                Joomla = {};
                Joomla.JText = strings;
            }
            else {
                Joomla.JText.load(strings);
            }
        })();
    </script>



</head>

<body id="thebody">

<div class="wrap">
    <nav class="navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle btn btn-danger" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <div style="display: inline-block;"><span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span></div>
                    <!-- <div style="display: inline-block;">Menu</div> -->
                </button>
                <a class="navbar-brand" href="/"><img src="/templates/bisous/images/logo.png" class="logo-img"></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <div class="moduletable_menu">
                    <ul class="nav navbar-nav">
                        <li class="item-143 current active"><a href="/" >Home</a></li><li class="item-101"><a href="/actueel" >Actueel</a></li><li class="item-267"><a class="actiebutton" href="/leenstelsel/overzicht" >Leenstelsel</a></li><li class="item-149"><a href="/studenten" >Voor Studenten</a></li><li class="item-205"><a href="/contact/international-students" >International</a></li><li class="item-150"><a href="/dossiers" >Dossiers</a></li><li class="item-154"><a href="/organisatie/overzicht" >Organisatie</a></li><li class="item-116"><a href="/contact/overzicht" >Contact</a></li></ul>
                </div>

                <ul class="nav navbar-nav navbar-right">
                    <li></li>
                    <li><a href="http://www.twitter.com/studentenbond" target="_blank"><img src="/images/template/Icon-Twitter.png"></a></li>
                    <li><a href="http://www.facebook.com/studentenbond" target="_blank"><img src="/images/template/Icon-Facebook.png"></a></li>
                    <li><a href="http://www.youtube.com/channel/UCLKUd8uaM5ztWHbfFYVyx5Q/feed" target="_blank"><img src="/images/template/Icon-Youtube.png"></a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container maincontainer">
        <div class="col-md-12 bisoustextblock itemView">

    <div class="page-header">
        <h1>Sloop het schuldenstelsel <strong><img src="http://stophetleenstelsel.frankquist.nl/img/twitter.png"></strong>-generator</h1>
    </div>
    <p class="lead">Ben je ook ontevreden, boos of verdrietig met de dreigende invoering van het leenstelsel? Gebruik dan deze tweet generator om een persoonlijke boodschap aan een kamerlid van keuze te sturen. Geef aan hoe je je voelt, kies waar je je zorgen over maakt en aan wie je dat wilt vertellen.
        Voor meer uitleg over de plannen ga je naar: <a href="http://www.lsvb.nl/leenstelsel/">LSVb.nl/leenstelsel</a></p>
    <div class="row">
    <div class="col-md-6">
        <h1>Opties</h1>
        <p>
            Ik ben:
            <div class="btn-group">
                <a href="javascript:switchMood('woedend')" class="btn btn-default" id="moodwoedend">woedend</a>
                <a href="javascript:switchMood('verbijsterd')" class="btn btn-default" id="moodverbijsterd">verbijsterd</a>
                <a href="javascript:switchMood('teleurgesteld')" class="btn btn-default" id="moodteleurgesteld">teleurgesteld</a>
                <a href="javascript:switchMood('verdrietig')" class="btn btn-default" id="moodverdrietig">verdrietig</a>
            </div> <br />over de

        <div class="btn-group">
            <a href="javascript:switchReason('toegankelijkheid')" class="btn btn-default btn-xs" id="reasontoegankelijkheid">vermindering van de toegankelijkheid</a>
            <a href="javascript:switchReason('schulden')" class="btn btn-default btn-xs" id="reasonschulden">invoering van een schuldenstelsel</a>
        </div>
        </p>
        <p>En ik wil graag tweeten naar:
        <div class="container">
            <div class="row">
        <div class="col-md-3">
            <a href="javascript:switchRecipient('jesseklaver')" class="thumbnail">
                <div class="caption" id="recipient_jesseklaver">
                    Jesse Klaver (GL)<br />
                    </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="javascript:switchRecipient('paul_van_meenen')" class="thumbnail">
                <div class="caption" id="recipient_paul_van_meenen">
                    Paul v. Meenen (D66)
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="javascript:switchRecipient('momohandis')" class="thumbnail">
                <div class="caption" id="recipient_momohandis">
                    Mohamed Mohandis (PvdA)
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a href="javascript:switchRecipient('pduisenberg')" class="thumbnail">
                <div class="caption" id="recipient_pduisenberg">
                    Pieter Duisenberg (VVD)
                </div>
            </a>
        </div>
        </div><div class="row"><br />
        <div class="col-md-3">
            <a href="javascript:switchRecipient('DiederikSamsom')" class="thumbnail">
                <div class="caption" id="recipient_DiederikSamsom">
                    Diederik Samson (PvdA)
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="javascript:switchRecipient('HalbeZijlstra')" class="thumbnail">
                <div class="caption" id="recipient_HalbeZijlstra">
                    Halbe Zijlstra (VVD)<br />
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="javascript:switchRecipient('APechthold')" class="thumbnail">
                <div class="caption" id="recipient_APechthold">
                    Alexander Pechtold (D66)
                </div>
            </a>
        </div>
        <div class="col-md-3">
            <a href="javascript:switchRecipient('BramvanOjikGL')" class="thumbnail">
                <div class="caption" id="recipient_BramvanOjikGL">
                    Bram van Ojik (GL)<br />
                </div>
            </a>
        </div>
            </div></div>
</p></div>
    <div class="col-md-6"><h1>Je tweet</h1>
        <blockquote>
            <p id="tweetPreview">Hier verschijnt je tweet zodra je begint met het selecteren van opties.</p>
        </blockquote>
    <a href="#" id="tweetLink" class="btn btn-primary">Selecteer eerst een optie</a>
</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script>
        var tweetText;

        var currentRecipient;
        var currentMood;
        var currentReason;

        var moods = ['woedend', 'verdrietig', 'teleurgesteld', 'verbijsterd'];
        var recipients = ['jesseklaver', 'paul_van_meenen'];
        var reasons = ['toegankelijkheid', 'schulden'];

        var templates = [];
        templates['toegankelijkheid'] = [];
        templates['schulden'] = [];

        templates['toegankelijkheid']['woedend'] = ['Ik vind het belachelijk dat de toegang tot het hoger onderwijs afhangt van de financiële draagkracht',
                                        'Het is ronduit achterlijk dat ik niet meer mag studeren van jou!',
                                        'Die leuke master kan ik dus wel vergeten door dit bizarre akkoord!'];
        templates['toegankelijkheid']['verbijsterd'] = ['Waarom mag ik niet meer studeren van jou?',
                                            'Ik kan er met mijn hoofd niet bij dat toegankelijkheid hoger onderwijs zo’n klap krijgt',
                                            'Moet ik na mijn bachelor nu maar gewoon gaan stoppen met studeren?'];
        templates['toegankelijkheid']['teleurgesteld'] = ['Die leuke studie zie ik nu helaas aan mijn neus voorbij gaan',
                                             'Al mijn mooie plannen voor een vervolgstudie overboord, wat een verspilde energie',
                                            'Waarom laat de overheid het in vredesnaam toe dat het hoger onderwijs zo ontoegankelijk wordt'];
        templates['toegankelijkheid']['verdrietig'] = ['Mijn motivatie om nog een studie te gaan volgen is echt tot een dieptepunt gedaald',
                                        'Met pijn in mijn hart zie ik die leuke studie aan mijn neus voorbij gaan'];

        templates['schulden']['woedend'] = ['Waarom moet ik me nu al tot over mijn nek in de schulden steken?', 'Ik wil geen molensteen van schulden om mijn nek!', 'Ik vind het schandalig dat jullie aanzetten tot schulden!'];
        templates['schulden']['verbijsterd'] = ['Ik kan niet begrijpen waarom ik nu enorme schulden moet maken', 'Hoe is het mogelijk dat de overheid lenen wil gaan promoten onder aankomend studenten?', 'Gaan we de student nu leren dat lenen een goede zaak is? #omgekeerdewereld'];
        templates['schulden']['teleurgesteld'] = ['Ik had niet verwacht dat het maken van schulden ooit als iets positiefs gezien kon worden', 'Ik heb het geloof in politieke partijen verloren, ga zelf eerst eens verder studeren', 'Nog tientallen jaren studieschuld gaan terug betalen, wat een deprimerend vooruitzicht'];
        templates['schulden']['verdrietig'] = ['Wat ontzettend triest voor diegene die keihard studeert en vervolgens eindigt met een enorme schuld', 'Een studie gaan volgen? Helaas dat is voor mij nu echt veel te duur geworden'];

        // OPTIONS
        var hashtag = 'levenlanglenen';
        var tweetButtonText = 'GO!'

        $(document).ready(function() {
            currentMood = moods[Math.floor(Math.random()*moods.length)];
            currentRecipient = recipients[Math.floor(Math.random()*recipients.length)];
            currentReason = reasons[Math.floor(Math.random()*reasons.length)];
            assembleTweet();
        });

        var assembleTweet = function assembleTweet ()
        {
            randomReason = templates[currentReason][currentMood][Math.floor(Math.random()*templates[currentReason][currentMood].length)];
            tweetText = '@' + currentRecipient + ' ' + randomReason;
        }

        var updatePreview = function updatePreview ()
        {
            var tweetPreview = $('#tweetPreview');
            tweetPreview.html(tweetText + ' #' + hashtag);
        }

        var updateTwitterLink = function updateTwitterLink ()
        {
            var tweetLink = $('#tweetLink');
            tweetLink.attr('href', 'https://twitter.com/intent/tweet?text=' + tweetText + '&hashtags=' + hashtag + '&related=studentenbond');
            tweetLink.html(tweetButtonText);
        }

        var resetMoodButtons = function resetMoodButtons ()
        {
            $('#moodverdrietig').removeClass('btn-warning');
            $('#moodwoedend').removeClass('btn-warning');
            $('#moodteleurgesteld').removeClass('btn-warning');
            $('#moodverbijsterd').removeClass('btn-warning');
        }

        var resetReasonButtons = function resetReasonButtons ()
        {
            $('#reasontoegankelijkheid').removeClass('btn-warning');
            $('#reasonschulden').removeClass('btn-warning');
        }

        var resetRecipientButtons = function resetRecipientButtons ()
        {
            $('#recipient_jesseklaver').removeClass('btn-warning');
            $('#recipient_paul_van_meenen').removeClass('btn-warning');
            $('#recipient_momohandis').removeClass('btn-warning');
            $('#recipient_pduisenberg').removeClass('btn-warning');
            $('#recipient_DiederikSamsom').removeClass('btn-warning');
            $('#recipient_HalbeZijlstra').removeClass('btn-warning');
            $('#recipient_APechthold').removeClass('btn-warning');
            $('#recipient_JesseKlaver').removeClass('btn-warning');
            $('#recipient_BramvanOjikGL').removeClass('btn-warning');
        }

        var switchMood = function switchMood (name)
        {
            currentMood = name;
            resetMoodButtons();
            resetReasonButtons();
            resetRecipientButtons();
            $('#mood'+currentMood ).addClass('btn-warning');
            $('#reason'+currentReason ).addClass('btn-warning');
            $('#recipient_'+currentRecipient ).addClass('btn-warning');
            assembleTweet();
            updatePreview();
            updateTwitterLink();
        }

        var switchReason = function switchReason (name)
        {
            currentReason = name;
            resetMoodButtons();
            resetReasonButtons();
            resetRecipientButtons();
            $('#mood'+currentMood ).addClass('btn-warning');
            $('#reason'+currentReason ).addClass('btn-warning');
            $('#recipient_'+currentRecipient ).addClass('btn-warning');
            assembleTweet();
            updatePreview();
            updateTwitterLink();
        }

        var switchRecipient = function switchRecipient (name)
        {
            currentRecipient = name;
            resetMoodButtons();
            resetReasonButtons();
            resetRecipientButtons();
            $('#mood'+currentMood ).addClass('btn-warning');
            $('#reason'+currentReason ).addClass('btn-warning');
            $('#recipient_'+name ).addClass('btn-warning');
            assembleTweet();
            updatePreview();
            updateTwitterLink();
        }

    </script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>